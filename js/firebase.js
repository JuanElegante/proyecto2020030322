//Configuración Firebase
// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-app.js";
import { getAuth } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-auth.js"
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyDMLmiznnAKDi6nk0R-01QFNqnw_0YJoLU",
  authDomain: "proyectohendra.firebaseapp.com",
  databaseURL: "https://proyectohendra-default-rtdb.firebaseio.com",
  projectId: "proyectohendra",
  storageBucket: "proyectohendra.appspot.com",
  messagingSenderId: "884095474742",
  appId: "1:884095474742:web:ea25781cde22d2a1824c97",
  measurementId: "G-2EJY7QXCKD"
};
// Initialize Firebase
export const app = initializeApp(firebaseConfig);
export const authentication = getAuth(app);