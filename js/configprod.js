//Configuración Firebase
import { onAuthStateChanged, signOut } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-auth.js"
import {getDatabase,ref,set,child,get,update,remove, onValue }from "https://www.gstatic.com/firebasejs/9.13.0/firebase-database.js"
import {getStorage, ref as refS, uploadBytes, getDownloadURL} from "https://www.gstatic.com/firebasejs/9.13.0/firebase-storage.js"
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-app.js"
import { getAuth } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-auth.js"
const firebaseConfig = {
  apiKey: "AIzaSyDMLmiznnAKDi6nk0R-01QFNqnw_0YJoLU",
  authDomain: "proyectohendra.firebaseapp.com",
  databaseURL: "https://proyectohendra-default-rtdb.firebaseio.com",
  projectId: "proyectohendra",
  storageBucket: "proyectohendra.appspot.com",
  messagingSenderId: "884095474742",
  appId: "1:884095474742:web:ea25781cde22d2a1824c97",
  measurementId: "G-2EJY7QXCKD"
};
// Initialize Firebase
const app = initializeApp(firebaseConfig);
const authentication = getAuth(app);
const db = getDatabase();

//objetos
var id = "";
var nombre = "";
var desc = "";
var precio = "";
var status = "";
var url = "";

//botones
var btnAgregar = document.getElementById("btnAgregar");
var btnActualizar = document.getElementById("btnActualizar");
var btnBorrar = document.getElementById("btnBorrar");
var btnMostrar = document.getElementById("btnMostrar");
var btnLimpiar = document.getElementById("btnLimpiar");
var btnSesion = document.getElementById("btnSesion");
var btnTodos = document.getElementById("btnTodos");

var lista = document.getElementById("lista");
var img = document.getElementById("img");
var articulos = document.getElementById('row');

var errID = document.getElementById("errorID");
var errNom = document.getElementById("errorNombre");
var errDesc = document.getElementById("errorDesc");
var errPrec = document.getElementById("errorPrecio");
var errImg = document.getElementById("errorImg");

//función para recargar productos (recargar página necesario)
if(window.location.href == "http://127.0.0.1:5500/html/producto.html"){
  window.onload = mostrarProductos();
}

function mostrarProductos(){
    const db = getDatabase();
    const dbRef = ref(db, 'productos');
    onValue(dbRef, (snapshot)=>{
        if(lista){
            lista.innerHTML = "";
        } 
        snapshot.forEach((childSnapshot) => {
            const childKey = childSnapshot.key;
            const childData = childSnapshot.val();
            if(lista){
                lista.innerHTML = lista.innerHTML + "<div class='registro'> " + "<h2 style='font-size: larger;'>Id: " + childKey + "</h2><h2 style='font-size: larger;'>Nombre: " + childData.nombre + "</h2>"+"<p style='font-size: larger;'> Estado:" + childData.status + "</p>"+"<img src='"+ childData.url +"'>";
            }else if(articulos){
                if(childData.status!=1){
                    //<div class="imgprod"><img src="/img/ArenaGato.png" alt="ArenaGato"><h4 style="font-size:larger;"><a href="producto.html">Arena para gato</a> $150.00</h4></div>
                    articulos.innerHTML= articulos.innerHTML + "<div class='imgprod'> <img src='"+childData.url+"'> <br> <h4 style='font-size: larger;'>"+childData.desc +"</h4><br><h4 style='font-size: larger;'>$"+childData.precio +"</h4><br></div>";
                }
            }
            
        });
    },{
        onlyOnce: true
    });

}

function leerInputs(){
    id = document.getElementById('id').value;
    nombre = document.getElementById('nombre').value;
    desc = document.getElementById('desc').value;
    precio = document.getElementById('precio').value;
    status = document.getElementById('status').value;
    url = document.getElementById('url').value;
}

function validarCampos(){
    subirArchivo();
    leerInputs();
    id == "" ? errID.style.visibility = "visible" : errID.style.visibility = "hidden";
    nombre == "" ? errNom.style.visibility = "visible" : errNom.style.visibility = "hidden";
    desc == "" ? errDesc.style.visibility = "visible" : errDesc.style.visibility = "hidden";
    precio == "" ? errPrec.style.visibility = "visible" : errPrec.style.visibility = "hidden";
    img == "" ? errImg.style.visibility = "visible" : errImg.style.visibility = "hidden";
    if(id == "" || nombre == "" || desc == "" || precio == "" || img == ""){
        return 0;
    }else{
        return 1;
    }
}

async function insertarDatos(){
    if(validarCampos()!=0){
        setTimeout(leerInputs,5000);
        setTimeout(()=>{
            set(ref(db,'productos/' + id),{
                nombre:nombre,
                desc:desc,
                precio:precio,
                status:status,
                url:url})
                .then((response) => {
                alert("Producto registrado con éxito!");
                mostrarProductos();
                })
                .catch((error) => {
                alert("Error en el registro...")
            });
        }, 5000);
    }else{
        alert("Ingrese todos los datos.");
    }
    limpiar();
}

async function mostrarDatos(){
    leerInputs();
    const dbref = ref(db);
    
    await get(child(dbref,'productos/' + id)).then((snapshot)=>{
        if (snapshot.exists()) {
            nombre = snapshot.val().nombre;
            desc = snapshot.val().desc;
            precio = snapshot.val().precio;
            status = snapshot.val().status;
            url = snapshot.val().url;
            escribirInputs();
            
        }else{
            alert("No existe el producto.");
        }
    }).catch((error)=>{
        alert("Ocurrió un error: " + error);
    });
}

/*async*/ function actualizar(){
    leerInputs();
    if(id!=""){
        if(nombre!="" || desc!="" || precio!="" || url!=""){
            subirArchivo();
            setTimeout(leerInputs,5000);
            setTimeout(()=>{
                update(ref(db,'productos/'+ id),{
                    nombre:nombre,
                    desc:desc,
                    precio:precio,
                    status:status,
                    url:url
                }).then(()=>{
                    alert("Actualizado con éxito!");
                    mostrarProductos();
                })
                .catch((error)=>{
                    alert("Ocurrió un error: " + error );
                });
            }, 5000);
        }else{
            alert("Ingrese datos para actualizar.")
        }
    }else{
        alert("Ingrese un ID para actualizar.")
    }
    limpiar();    
}

function escribirInputs(){
    document.getElementById('id').value = id;
    document.getElementById('nombre').value = nombre;
    document.getElementById('desc').value = desc;
    document.getElementById('precio').value = precio;
    document.getElementById('status').value = status;
    document.getElementById('url').value = url;
}

async function borrar(){
    leerInputs();
    const dbref = ref(db);
    
    if(id!=""){
        await get(child(dbref,'productos/' + id)).then((snapshot)=>{
            if (snapshot.exists()) {
                update(ref(db,'productos/'+ id),{
                    status:"1"
                   }).then(()=>{
                    alert("Se deshabilitó el registro.");
                    mostrarProductos();
                   })
                   .catch((error)=>{
                    alert("Se encontró un error. " + error );
                   });
                
            }else{
                alert("No existe el producto.");
            }
        }).catch((error)=>{
            alert("Se encontró un error: " + error);
        });
    } else{
        alert("No existe el producto.");
    }
}

function limpiar(){
    lista.innerHTML="";
    id="";
    nombre="";
    desc="";
    precio="";
    status=0;
    url="";
    document.getElementById('mostrarImg').src="";
    escribirInputs();
}

var file="";
var name="";

async function cargarImagen(){
    // archivo seleccionado
    file = event.target.files[0];
    name = event.target.files[0].name;
}

async function subirArchivo(){
    const storage = getStorage();
    const storageRef = refS(storage, 'imagenes/' + name);
    if(url != ""){
        await uploadBytes(storageRef, file).then((snapshot) => {
            alert("Se cargo el archivo");
        });
        descargarImagen();
    }
}

async function descargarImagen(){
    const storage = getStorage();
    const storageRef = refS(storage, 'imagenes/'+name);
    await getDownloadURL(storageRef)
    .then((url) => {
    document.getElementById('url').value = url;
    })
    .catch((error) => {
        switch (error.code) {
            case 'storage/object-not-found':
            alert("No existe el archivo: " + error );
            break;
            case 'storage/unauthorized':
            alert("No hay autorización para acceder: " + error );
            break;
            case 'storage/canceled':
            alert("Ha cancelado la subida del archivo: " + error );
            break;
            case 'storage/unknown':
            alert("Ocurrió un error: " + error );
            break;
        }
    });
}
if(img){
    img.addEventListener('change',cargarImagen);
}
// Codificar evento click
if(btnAgregar){
    btnAgregar.addEventListener('click', insertarDatos);
}
if(btnMostrar){
    btnMostrar.addEventListener('click',mostrarDatos);
}
if(btnActualizar){
    btnActualizar.addEventListener('click',actualizar);
}
if(btnBorrar){
    btnBorrar.addEventListener('click',borrar);
}
if(btnTodos){
    btnTodos.addEventListener('click', mostrarProductos);
}
if(btnLimpiar){
    btnLimpiar.addEventListener('click', limpiar);
}
if(btnSesion){
    btnSesion.addEventListener('click', async ()=>{
        await signOut(authentication);
        location = "/html/indexadmin.html"
    })
}