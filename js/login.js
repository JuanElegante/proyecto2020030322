import { authentication } from "/js/firebase.js"
import { signInWithEmailAndPassword } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-auth.js"

var form = document.getElementById("formLogin");

form.addEventListener('submit', async e=>{
    e.preventDefault();
    const mail = form['user'].value;
    const pass = form['pass'].value;
    try{
        const credenciales = await signInWithEmailAndPassword(authentication, mail, pass);
        alert("Bienvenido.");
        location = '/html/admin.html';
    }catch(error){
        alert("Usuario y/o contraseña incorrectos.");
    }
})